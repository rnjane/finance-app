from django.shortcuts import render
from django.views.generic import TemplateView

class Index(TemplateView):
    def get(self, request, **kwargs):
        return render(request, 'index.html', context=None)

class Register(TemplateView):
    template_name = "register.html"

class Login(TemplateView):
    template_name = "login.html"