from django.conf.urls import url
from financeapp import views

urlpatterns = [
    url(r'^$', views.Index.as_view(), name='home'),
    url(r'^register/$', views.Register.as_view(), name='register'),
    url(r'^login/$', views.Login.as_view(), name='login'),
]